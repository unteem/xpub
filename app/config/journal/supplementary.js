export default {
  description:
    '<pre>There are 3 types of item which should be uploaded here:<br />' +
    '<ul>' +
    '<li>Regular figures which should also be be embedded in the manuscript.(These are helpful for the production<br />process,if your article is accepted, in case there are any issues with the embedded versions.)</li>' +
    '<li>Supplemental items that provide a relevant and useful expansion of the article (Examples include appendices,<br/>very large tables, audios, videos, three-dimensional visualizations, interactive graphics, and so on.)</li>' +
    '<li>Items that support the peer review process. (Examples are ported reviews and decision letters for use during<br />the Streamlined Review workflow. Also, if you have a long cover letter you would rather upload than paste<br/>into the text box, please also upload it here.)</li>' +
    '</ul>' +
    'Supplemental materials can be named in almost any way, provided that the files are clearly and consistently named,<br />are uploaded in chronological order, and grouped as they are described above. For example, a typical article<br/>might include:' +
    '<br />' +
    '<ul>' +
    '<li>Figure 1.jpg</li>' +
    '<li>Figure 2.jpg</li>' +
    '<li>Figure 3.jpg</li>' +
    '<li>Supplemental Table 1.docx</li>' +
    '<li>Ported Decision Letter and Reviews from Journal X.docx</li>' +
    '</ul>' +
    '</pre>',
}
